const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    minifyCSS = require('gulp-minify-css');

gulp.task('sass', () => {
    gulp.src('./scss/*.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(autoprefixer({
            versions: ['lats 2 browsers']
        }))
        .pipe(gulp.dest('./css'));
});

gulp.task('default', () => {
    browserSync.init({
        proxy: 'http://localhost/rejugados/'
    });
    gulp.watch('./*.html').on('change', browserSync.reload);
    gulp.watch('./css/*.css').on('change', browserSync.reload);
    gulp.watch('./js/*.js').on('change', browserSync.reload);
    gulp.watch('./img/**').on('change', browserSync.reload);
    gulp.watch('./scss/*.scss', ['sass']);
});