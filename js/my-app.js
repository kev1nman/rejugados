$(document).ready(function () {
    $('div').click(function () {
        var url = $(this).attr('rel');
        $('#iframe').attr('src', url);
        $('#iframe').reload();
    });

    $("img.lazyload").lazyload({
        effect: 'fadeIn'
    });
}); 
    

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        dots: false,
        autoplay: true,
        responsive: {
            0: {
                items: 1
            }
        }
    })

    
